﻿using System;
using System.IO;

namespace Trains
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new Program(args).TryRun();
        }

        private readonly string[] _args;
        private string _trainFilePath = "";
        private char _destination = char.MinValue;

        private Program(string[] args)
        {
            _args = args;
            TryParseTrainLinesFilePathFromArgs(args);
            TryParseDestinationFromArgs(args);
        }

        private void TryParseTrainLinesFilePathFromArgs(string[] args)
        {
            if (args != null && args.Length > 0 && !string.IsNullOrEmpty(args[0]))
                _trainFilePath = args[0];
        }

        private void TryParseDestinationFromArgs(string[] args)
        {
            if (args != null && args.Length > 1 && !string.IsNullOrEmpty(args[1]))
                _destination = args[1].Trim()[0];
        }

        private void TryRun()
        {
            try
            {
                Run();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }

        private void Run()
        {
            ShowHeader();

            while (IsTrainFileNotSet())
                AskForTrainFile();

            while (IsDestinationNotSet())
                AskForDestination();

            ShowMoves();

            if (IsInteractive())
                ShowFooter();
        }

        private static void ShowHeader()
        {
            Console.WriteLine("Trains !");
        }

        private bool IsTrainFileNotSet()
        {
            return string.IsNullOrEmpty(_trainFilePath);
        }

        private void AskForTrainFile()
        {
            Console.WriteLine("Enter the path of the train lines file:");
            Console.Write("> ");

            _trainFilePath = Console.ReadLine() ?? "";
        }

        private bool IsDestinationNotSet()
        {
            return _destination == char.MinValue;
        }

        private void AskForDestination()
        {
            Console.WriteLine("Enter the destination:");
            Console.Write("> ");

            var destination = Console.ReadLine() ?? "";
            destination = destination.Trim();

            if (destination.Length > 0)
                _destination = destination[0];
        }

        private void ShowMoves()
        {
            var trainLines = File.ReadAllLines(_trainFilePath);
            var trainsStarter = new TrainsStarter();
            var moves = trainsStarter.Start(trainLines, _destination);

            Console.WriteLine("Moves:");
            Console.WriteLine(moves);
        }

        private bool IsInteractive()
        {
            return _args != null && _args.Length == 0;
        }

        private static void ShowFooter()
        {
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
        }
    }
}