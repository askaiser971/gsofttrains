﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trains
{
    public class TrainsStarter : ITrainsStarter
    {
        /// <summary>
        /// Cette méthode sera appelée par les tests.
        /// </summary>
        /// <param name="trainLines">Un array de string avec les lignes de trains.</param>
        /// <param name="destination">La lettre de la destination.</param>
        /// <returns>Retourne une liste de mouvements à faire pour obtenir la solution.</returns>
        public string Start(string[] trainLines, char destination)
        {
            var sorter = new TrainSorter(trainLines, destination);
            var moves = sorter.TrySort();
            return FormatMoves(moves);
        }

        private static string FormatMoves(IEnumerable<CarMove> moves)
        {
            return string.Join(
                ";", moves.Select(m => $"{m.Cars},{m.From},{m.To}")
            );
        }
    }

    internal class TrainSorter
    {
        private const int MaxIterations = 10000;
        private const int MaxCarChunkLength = 3;

        private readonly List<TrainLine> _sortingLines;
        private readonly TrainLine _trainLine;
        private readonly List<CarMove> _moves;
        private readonly char _destination;

        private TrainLine _srcLine;
        private TrainLine _destLine;

        public TrainSorter(string[] sortingLines, char destination)
        {
            if (sortingLines == null)
                throw new ArgumentNullException(nameof(sortingLines));

            _sortingLines = new List<TrainLine>();
            _trainLine = new TrainLine(0, "");
            _moves = new List<CarMove>();
            _destination = char.ToUpper(destination);

            for (var i = 0; i < sortingLines.Length; i++)
                _sortingLines.Add(new TrainLine(i + 1, sortingLines[i].ToUpper()));
        }

        public IEnumerable<CarMove> TrySort()
        {
            try
            {
                return Sort();
            }
            catch (SortingImpossibleException)
            {
                return Enumerable.Empty<CarMove>();
            }
        }

        private IEnumerable<CarMove> Sort()
        {
            for (var iter = 1; HasDestinationCarsToMove() && iter <= MaxIterations; iter++)
                MoveNextCars();

            if (HasDestinationCarsToMove())
                throw new SortingImpossibleException($"Could not solve train sorting after {MaxIterations} iterations");

            return _moves;
        }

        private bool HasDestinationCarsToMove()
        {
            return _sortingLines.Any(l => !l.IsClosed && l.Contains(_destination));
        }

        private void MoveNextCars()
        {
            _srcLine = GetBestSourceLine();
            _destLine = FindBestDestinationLine();
            if (_destLine == null)
                throw new SortingImpossibleException("Could not find an available train sorting line");

            var carToMoveCount = GetHowManyCarsToMove();
            var movedCars = _srcLine.MoveCarsTo(_destLine, carToMoveCount);

            AddMoves(movedCars);
        }

        private TrainLine GetBestSourceLine()
        {
            return _sortingLines
                .Where(IsNotClosed)
                .OrderByDescending(HasDestinationCar)
                .ThenByDescending(HasAvailableDestinationLine)
                .ThenBy(NumberOfMovesToDestinationCar)
                .ThenByDescending(NumberOfCarsToDestinationCar)
                .ThenBy(l => l.Position)
                .First();
        }

        private TrainLine FindBestDestinationLine()
        {
            var distance = _srcLine.GetDistanceToCar(_destination);
            if (distance == 0)
                return _trainLine;

            return _sortingLines
                .Where(IsDestinationLineWithAtLeastOneSpace)
                .OrderBy(HasDestinationCar)
                .ThenByDescending(GetNumberOfMovesAvailable)
                .ThenBy(GetProximityToSourceLine)
                .ThenBy(l => l.Length)
                .FirstOrDefault();
        }

        private bool IsNotClosed(TrainLine line)
        {
            return !line.IsClosed;
        }

        private bool HasDestinationCar(TrainLine line)
        {
            return line.Contains(_destination);
        }

        private bool HasAvailableDestinationLine(TrainLine line)
        {
            var distanceToCar = line.GetDistanceToCar(_destination);
            var potentialDestLines = _sortingLines.Where(l => l != line);
            var availableDestLines = potentialDestLines.Where(l => l.AvailableSpace >= distanceToCar);
            return availableDestLines.Any();
        }

        private int NumberOfMovesToDestinationCar(TrainLine line)
        {
            return line.GetDistanceToCar(_destination) / MaxCarChunkLength;
        }

        private bool NumberOfCarsToDestinationCar(TrainLine line)
        {
            return line.GetDistanceToCar(_destination) == 0;
        }

        private int GetProximityToSourceLine(TrainLine line)
        {
            return Math.Abs(_srcLine.Position - line.Position);
        }

        private bool IsDestinationLineWithAtLeastOneSpace(TrainLine line)
        {
            return line != _srcLine && line.AvailableSpace > 0;
        }

        private int GetNumberOfMovesAvailable(TrainLine line)
        {
            return line.AvailableSpace / MaxCarChunkLength;
        }

        private int GetHowManyCarsToMove()
        {
            var distance = _destLine == _trainLine
                ? _srcLine.GetCountAtBeginningOfType(_destination)
                : Math.Min(_srcLine.GetDistanceToCar(_destination), _destLine.AvailableSpace);
            
            return Math.Min(distance, MaxCarChunkLength);
        }

        private void AddMoves(List<char> movedCars)
        {
            var carsStr = new string(movedCars.ToArray());
            _moves.Add(new CarMove(carsStr, _srcLine.Position, _destLine.Position));
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var line in _sortingLines)
                sb.AppendLine(line.ToString());

            sb.AppendLine(_trainLine.ToString());
            return sb.ToString();
        }

        private class SortingImpossibleException : Exception
        {
            public SortingImpossibleException(string message)
                : base(message)
            { }
        }
    }
}