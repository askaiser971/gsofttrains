﻿namespace Trains
{
    internal class CarMove
    {
        public string Cars { get; }
        public int From { get; }
        public int To { get; }

        public CarMove(string cars, int from, int to)
        {
            Cars = cars;
            From = from;
            To = to;
        }

        public override string ToString()
        {
            return $"{Cars},{From},{To}";
        }
    }
}