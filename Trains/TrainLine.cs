﻿using System;
using System.Collections.Generic;

namespace Trains
{
    public class TrainLine
    {
        private const char EmptyCar = '0';

        private readonly int _maxLength;
        private readonly bool _isClosed;
        private readonly Stack<char> _cars;

        public int Position { get; }

        public int Length
        {
            get { return _cars.Count; }
        }

        public int AvailableSpace
        {
            get { return _isClosed ? 0 : _maxLength - Length; }
        }

        public bool IsClosed
        {
            get { return _isClosed; }
        }

        public TrainLine(int position, string line)
        {
            if (line == null)
                throw new ArgumentNullException(nameof(line));

            _maxLength = line.Length;
            _isClosed = line.StartsWith("X");
            
            var trimmedLine = line.TrimStart(EmptyCar);
            if (trimmedLine.Length > _maxLength)
                throw new ArgumentOutOfRangeException($"Line #{position} exceeded {_maxLength} cars: {line}");

            Position = position;
            _cars = new Stack<char>();
            for (var i = trimmedLine.Length - 1; i >= 0; i--)
                _cars.Push(trimmedLine[i]);
        }

        public int GetDistanceToCar(char carToFind)
        {
            var distance = 0;
            foreach (var car in _cars)
            {
                if (car == carToFind)
                    return distance;

                if (car != EmptyCar)
                    distance++;
            }

            return -1;
        }

        public List<char> MoveCarsTo(TrainLine other, int count)
        {
            if (count > Length)
                throw new InvalidOperationException($"Requested to move {count} cars but only {Length} available");

            var movedCars = new List<char>(count);
            for (var i = 0; i < count; i++)
                movedCars.Add(_cars.Pop());

            movedCars.Reverse();
            foreach (var car in movedCars)
                other._cars.Push(car);

            movedCars.Reverse();
            RemoveEmptyCarsAtBeginning();
            
            return movedCars;
        }

        public bool Contains(char car)
        {
            return _cars.Contains(car);
        }

        public int GetCountAtBeginningOfType(char carToFind)
        {
            int count;
            var cars = new List<char>(_cars);
            for (count = 0; count < cars.Count; count++)
                if (cars[count] != carToFind)
                    break;

            return count;
        }

        private void RemoveEmptyCarsAtBeginning()
        {
            while (_cars.Count > 0 && _cars.Peek() == EmptyCar)
                _cars.Pop();
        }

        public override string ToString()
        {
            var cars = new string(_cars.ToArray()).PadLeft(_maxLength, EmptyCar);
            return $"{cars} #{Position}";
        }
    }
}