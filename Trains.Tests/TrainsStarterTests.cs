﻿using System;
using NUnit.Framework;

namespace Trains.Tests
{
    [TestFixture]
    public class TrainsStarterTests
    {
        private ITrainsStarter _trainStarter;

        [SetUp]
        public void Setup()
        {
            _trainStarter = new TrainsStarter();
        }

        [Test]
        public void NullTrainLines_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => AssertSortingMoves(null, 'A', ""));
        }

        [Test]
        public void ImpossibleSorting_ReturnsEmpty()
        {
            AssertSortingImpossible(new string[0], 'A');
            AssertSortingImpossible(new[] { "0000000000" }, 'A');
            AssertSortingImpossible(new[] { "000000000B" }, 'A');
            AssertSortingImpossible(new[] { "0000000BBA" }, 'A');
        }

        [Test]
        public void SimpleSorting_RequiresOneMove()
        {
            AssertSortingMoves(new[] { "000000000A" }, 'A', "A,1,0");
            AssertSortingMoves(new[] { "000000000A", "0000000000" }, 'A', "A,1,0");
            AssertSortingMoves(new[] { "00000000AB", "0000000000" }, 'A', "A,1,0");
        }

        [Test]
        public void GetRidOfUnusedCars_ToAccessDestinationCars()
        {
            AssertSortingMoves(new[] { "00000000BA", "0000000000" }, 'A', "B,1,2;A,1,0");

            AssertSortingMoves(
                new[] { "00000000BA", "000000000A", "0000000000" },
                'A',
                "A,2,0;B,1,2;A,1,0"
            );

            AssertSortingMoves(new[] { "00000000BA", "00000000AB" }, 'A', "A,2,0;B,1,2;A,1,0");
            AssertSortingMoves(new[] { "00000000BA", "00000000BA" }, 'A', "B,1,2;A,1,0;BB,2,1;A,2,0");
            AssertSortingMoves(new[] { "000000CABA", "0000000000" }, 'A', "C,1,2;A,1,0;B,1,2;A,1,0");
        }

        [Test]
        public void GetRidOfUnusedCars_ToNearestTrainLines()
        {
            AssertSortingMoves(
                new[] { "0000000000", "0000000000", "00000000BA", "0000000000" },
                'A',
                "B,3,2;A,3,0"
            );
        }

        [Test]
        public void NotEnoughSpaceLeftToSort_ReturnsEmpty()
        {
            AssertSortingImpossible(new[] { "BBBBBBBBBA", "BBBBBBBBBB" }, 'A');
            AssertSortingImpossible(new[] { "0BBBBBBBBA", "0BBBBBBBBB" }, 'A');
        }

        [Test]
        public void WhenSpaceAvailable_MoveUnusedCarsByGroups()
        {
            AssertSortingMoves(new[] { "0000000BBA", "0000000000" }, 'A', "BB,1,2;A,1,0");
            AssertSortingMoves(new[] { "00000BBBBA", "0000000000" }, 'A', "BBB,1,2;B,1,2;A,1,0");
        }

        [Test]
        public void WhenSpaceAvailable_MoveDestinationCarsByGroups()
        {
            AssertSortingMoves(new[] { "0000000BAA", "0000000000" }, 'A', "B,1,2;AA,1,0");
            AssertSortingMoves(new[] { "00000BAAAA", "0000000000" }, 'A', "B,1,2;AAA,1,0;A,1,0");
        }

        [Test]
        public void TrainLine_HasUnlimitedSpace()
        {
            AssertSortingMoves(
                new[] { "AAAAAAAAAA", "000000000A" },
                'A', "AAA,1,0;AAA,1,0;AAA,1,0;A,1,0;A,2,0"
            );
        }

        [Test]
        public void SortingLinesWithSpacesBetweenCars_BreaksGroups()
        {
            AssertSortingMoves(new[] { "00000AA00A" }, 'A', "AA,1,0;A,1,0");
            AssertSortingMoves(
                new[] { "A00AA0B00A", "0000000000" },
                'A',
                "A,1,0;AA,1,0;B,1,2;A,1,0"
            );
        }

        [Test]
        public void WhenLowSpaceAvailable_SplitGroupsOfUnusedCars()
        {
            AssertSortingMoves(
                new[] { "0000000BBA", "0BBBBBBBBB", "0BBBBBBBBB" },
                'A',
                "B,1,2;B,1,3;A,1,0"
            );

            AssertSortingMoves(
                new[] { "BBABBBBBBB", "0BBBBBBBBB", "0BBBBBBBBB" },
                'A',
                "B,1,2;B,1,3;A,1,0"
            );
        }

        [Test]
        public void GsoftTestCase1()
        {
            AssertSortingMoves(
                new[] { "0000ACDGC", "0000000DG" },
                'C',
                "A,1,2;C,1,0;DG,1,2;C,1,0"
            );
        }

        [Test]
        public void GsoftTestCase2()
        {
            AssertSortingMoves(
                new[] { "0000AGCAG", "00DCACGDG" },
                'C',
                "AG,1,2;C,1,0;AGD,2,1;C,2,0;A,2,1;C,2,0"
            );
        }

        [Test]
        public void LotsOfSortingLines_HalfFull_IsSortingPossible()
        {
            AssertSortingPossible(new[]
            {
                "00000DGCDG",
                "0000ACCACC",
                "000CDGADGA",
                "000DGDGADG",
                "0000AADGAD",
                "000ACGDCGD"
            }, 'G');
        }

        [Test]
        public void LotsOfSortingLines_AlmostFull_IsSortingPossible()
        {
            AssertSortingPossible(new[]
            {
                "00DGCDGEFA",
                "0ACCACCGDE",
                "CDGADGADEE",
                "DGDGADGCAA",
                "0AADGADCGD",
                "ACGDCGDEGD"
            }, 'G');
        }
        
        [Test]
        public void SortingLines_HaveTheirOwnMaxLength()
        {
            AssertSortingMoves(new[] { "BBA", "0B", "0B" }, 'A', "B,1,2;B,1,3;A,1,0");
        }
        
        [Test]
        public void ClosedSortingLines_ShouldBeIgnored()
        {
            AssertSortingMoves(new[] { "BBA", "X00", "00" }, 'A', "BB,1,3;A,1,0");
            AssertSortingMoves(new[] { "BBA", "X00A", "00" }, 'A', "BB,1,3;A,1,0");
        }

        private void AssertSortingMoves(string[] trainLines, char destination, string expectedMoves)
        {
            var actualMoves = _trainStarter.Start(trainLines, destination);
            Assert.That(actualMoves, Is.EqualTo(expectedMoves));
        }

        private void AssertSortingPossible(string[] trainLines, char destination)
        {
            var actualMoves = _trainStarter.Start(trainLines, destination);
            Assert.NotNull(actualMoves);
            Assert.That(actualMoves, Has.Length.GreaterThan(0));
        }

        private void AssertSortingImpossible(string[] trainLines, char destination)
        {
            var actualMoves = _trainStarter.Start(trainLines, destination);
            Assert.NotNull(actualMoves);
            Assert.That(actualMoves, Has.Length.EqualTo(0));
        }
    }
}