﻿using System;
using NUnit.Framework;

namespace Trains.Tests
{
    [TestFixture]
    public class TrainLineTests
    {
        [Test]
        public void NotEnoughCarsToMove_ThrowsException()
        {
            var a = new TrainLine(0, "000000000A");
            var b = new TrainLine(0, "0000000000");

            Assert.Throws<InvalidOperationException>(
                () => a.MoveCarsTo(b, 2)
            );
        }

        [Test]
        public void Position()
        {
            var line = new TrainLine(1, "00000000AB");
            Assert.That(line.Position, Is.EqualTo(1));
        }

        [Test]
        public void ContainsSpecificCar()
        {
            var line = new TrainLine(0, "00000000AB");
            Assert.That(line.Contains('B'), Is.True);
        }

        [Test]
        public void DistanceToCar()
        {
            var line = new TrainLine(0, "00000000AB");
            Assert.That(line.GetDistanceToCar('B'), Is.EqualTo(1));
        }

        [Test]
        public void CountOfSameCarsAtTheBeginning()
        {
            var line = new TrainLine(0, "00000000AAB");
            Assert.That(line.GetCountAtBeginningOfType('A'), Is.EqualTo(2));
        }
    }
}